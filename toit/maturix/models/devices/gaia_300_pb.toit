// Code generated by protoc-gen-toit. DO NOT EDIT.
// source: maturix/models/devices/gaia_300.proto

import encoding.protobuf as _protobuf
import core as _core
import protogen.google.protobuf.timestamp_pb as _timestamp

// MESSAGE START: .maturix.models.devices.UplinkData
class UplinkData extends _protobuf.Message:
  timestamp/_core.Time := _protobuf.TIME_ZERO_EPOCH
  device_id/string := ""
  temperature/float := 0.0
  battery_strength/float := 0.0

  constructor
      --timestamp/_core.Time?=null
      --device_id/string?=null
      --temperature/float?=null
      --battery_strength/float?=null:
    if timestamp != null:
      this.timestamp = timestamp
    if device_id != null:
      this.device_id = device_id
    if temperature != null:
      this.temperature = temperature
    if battery_strength != null:
      this.battery_strength = battery_strength

  constructor.deserialize r/_protobuf.Reader:
    r.read_message:
      r.read_field 1:
        timestamp = _protobuf.deserialize_timestamp r
      r.read_field 10:
        device_id = r.read_primitive _protobuf.PROTOBUF_TYPE_STRING
      r.read_field 11:
        temperature = r.read_primitive _protobuf.PROTOBUF_TYPE_FLOAT
      r.read_field 12:
        battery_strength = r.read_primitive _protobuf.PROTOBUF_TYPE_FLOAT

  serialize w/_protobuf.Writer --as_field/int?=null --oneof/bool=false -> none:
    w.write_message_header this --as_field=as_field --oneof=oneof
    _protobuf.serialize_timestamp timestamp w --as_field=1
    w.write_primitive _protobuf.PROTOBUF_TYPE_STRING device_id --as_field=10
    w.write_primitive _protobuf.PROTOBUF_TYPE_FLOAT temperature --as_field=11
    w.write_primitive _protobuf.PROTOBUF_TYPE_FLOAT battery_strength --as_field=12

  num_fields_set -> int:
    return ((_protobuf.time_is_zero_epoch timestamp) ? 0 : 1)
      + (device_id.is_empty ? 0 : 1)
      + (temperature == 0.0 ? 0 : 1)
      + (battery_strength == 0.0 ? 0 : 1)

  protobuf_size -> int:
    return (_protobuf.size_timestamp timestamp --as_field=1)
      + (_protobuf.size_primitive _protobuf.PROTOBUF_TYPE_STRING device_id --as_field=10)
      + (_protobuf.size_primitive _protobuf.PROTOBUF_TYPE_FLOAT temperature --as_field=11)
      + (_protobuf.size_primitive _protobuf.PROTOBUF_TYPE_FLOAT battery_strength --as_field=12)

// MESSAGE END: .maturix.models.devices.UplinkData

// MESSAGE START: .maturix.models.devices.TypeKProbe
class TypeKProbe extends _protobuf.Message:
  timestamp/_core.Time := _protobuf.TIME_ZERO_EPOCH
  device_id/string := ""
  probe_index/int := 0
  raw_probe_value/float := 0.0

  constructor
      --timestamp/_core.Time?=null
      --device_id/string?=null
      --probe_index/int?=null
      --raw_probe_value/float?=null:
    if timestamp != null:
      this.timestamp = timestamp
    if device_id != null:
      this.device_id = device_id
    if probe_index != null:
      this.probe_index = probe_index
    if raw_probe_value != null:
      this.raw_probe_value = raw_probe_value

  constructor.deserialize r/_protobuf.Reader:
    r.read_message:
      r.read_field 1:
        timestamp = _protobuf.deserialize_timestamp r
      r.read_field 10:
        device_id = r.read_primitive _protobuf.PROTOBUF_TYPE_STRING
      r.read_field 11:
        probe_index = r.read_primitive _protobuf.PROTOBUF_TYPE_INT32
      r.read_field 12:
        raw_probe_value = r.read_primitive _protobuf.PROTOBUF_TYPE_FLOAT

  serialize w/_protobuf.Writer --as_field/int?=null --oneof/bool=false -> none:
    w.write_message_header this --as_field=as_field --oneof=oneof
    _protobuf.serialize_timestamp timestamp w --as_field=1
    w.write_primitive _protobuf.PROTOBUF_TYPE_STRING device_id --as_field=10
    w.write_primitive _protobuf.PROTOBUF_TYPE_INT32 probe_index --as_field=11
    w.write_primitive _protobuf.PROTOBUF_TYPE_FLOAT raw_probe_value --as_field=12

  num_fields_set -> int:
    return ((_protobuf.time_is_zero_epoch timestamp) ? 0 : 1)
      + (device_id.is_empty ? 0 : 1)
      + (probe_index == 0 ? 0 : 1)
      + (raw_probe_value == 0.0 ? 0 : 1)

  protobuf_size -> int:
    return (_protobuf.size_timestamp timestamp --as_field=1)
      + (_protobuf.size_primitive _protobuf.PROTOBUF_TYPE_STRING device_id --as_field=10)
      + (_protobuf.size_primitive _protobuf.PROTOBUF_TYPE_INT32 probe_index --as_field=11)
      + (_protobuf.size_primitive _protobuf.PROTOBUF_TYPE_FLOAT raw_probe_value --as_field=12)

// MESSAGE END: .maturix.models.devices.TypeKProbe

