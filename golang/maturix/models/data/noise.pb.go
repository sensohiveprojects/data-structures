// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.14.0
// source: maturix/models/data/noise.proto

package data

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Electricity struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Timestamp *timestamppb.Timestamp `protobuf:"bytes,1,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
	DeviceId  string                 `protobuf:"bytes,2,opt,name=device_id,json=deviceId,proto3" json:"device_id,omitempty"`
	Provider  string                 `protobuf:"bytes,3,opt,name=provider,proto3" json:"provider,omitempty"`
	OwnerUuid string                 `protobuf:"bytes,10,opt,name=owner_uuid,json=ownerUuid,proto3" json:"owner_uuid,omitempty"`
	Wattage   float64                `protobuf:"fixed64,12,opt,name=wattage,proto3" json:"wattage,omitempty"`
	Voltage   float64                `protobuf:"fixed64,13,opt,name=voltage,proto3" json:"voltage,omitempty"`
	Current   float64                `protobuf:"fixed64,14,opt,name=current,proto3" json:"current,omitempty"`
}

func (x *Electricity) Reset() {
	*x = Electricity{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maturix_models_data_noise_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Electricity) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Electricity) ProtoMessage() {}

func (x *Electricity) ProtoReflect() protoreflect.Message {
	mi := &file_maturix_models_data_noise_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Electricity.ProtoReflect.Descriptor instead.
func (*Electricity) Descriptor() ([]byte, []int) {
	return file_maturix_models_data_noise_proto_rawDescGZIP(), []int{0}
}

func (x *Electricity) GetTimestamp() *timestamppb.Timestamp {
	if x != nil {
		return x.Timestamp
	}
	return nil
}

func (x *Electricity) GetDeviceId() string {
	if x != nil {
		return x.DeviceId
	}
	return ""
}

func (x *Electricity) GetProvider() string {
	if x != nil {
		return x.Provider
	}
	return ""
}

func (x *Electricity) GetOwnerUuid() string {
	if x != nil {
		return x.OwnerUuid
	}
	return ""
}

func (x *Electricity) GetWattage() float64 {
	if x != nil {
		return x.Wattage
	}
	return 0
}

func (x *Electricity) GetVoltage() float64 {
	if x != nil {
		return x.Voltage
	}
	return 0
}

func (x *Electricity) GetCurrent() float64 {
	if x != nil {
		return x.Current
	}
	return 0
}

type ElectricityCost struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	OwnerUuid string                 `protobuf:"bytes,1,opt,name=owner_uuid,json=ownerUuid,proto3" json:"owner_uuid,omitempty"`
	Timestamp *timestamppb.Timestamp `protobuf:"bytes,10,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
}

func (x *ElectricityCost) Reset() {
	*x = ElectricityCost{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maturix_models_data_noise_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ElectricityCost) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ElectricityCost) ProtoMessage() {}

func (x *ElectricityCost) ProtoReflect() protoreflect.Message {
	mi := &file_maturix_models_data_noise_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ElectricityCost.ProtoReflect.Descriptor instead.
func (*ElectricityCost) Descriptor() ([]byte, []int) {
	return file_maturix_models_data_noise_proto_rawDescGZIP(), []int{1}
}

func (x *ElectricityCost) GetOwnerUuid() string {
	if x != nil {
		return x.OwnerUuid
	}
	return ""
}

func (x *ElectricityCost) GetTimestamp() *timestamppb.Timestamp {
	if x != nil {
		return x.Timestamp
	}
	return nil
}

var File_maturix_models_data_noise_proto protoreflect.FileDescriptor

var file_maturix_models_data_noise_proto_rawDesc = []byte{
	0x0a, 0x1f, 0x6d, 0x61, 0x74, 0x75, 0x72, 0x69, 0x78, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73,
	0x2f, 0x64, 0x61, 0x74, 0x61, 0x2f, 0x6e, 0x6f, 0x69, 0x73, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x13, 0x6d, 0x61, 0x74, 0x75, 0x72, 0x69, 0x78, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c,
	0x73, 0x2e, 0x64, 0x61, 0x74, 0x61, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d,
	0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xed, 0x01, 0x0a, 0x0b, 0x45, 0x6c, 0x65, 0x63,
	0x74, 0x72, 0x69, 0x63, 0x69, 0x74, 0x79, 0x12, 0x38, 0x0a, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73,
	0x74, 0x61, 0x6d, 0x70, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d,
	0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d,
	0x70, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x64, 0x12, 0x1a,
	0x0a, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x12, 0x1d, 0x0a, 0x0a, 0x6f, 0x77,
	0x6e, 0x65, 0x72, 0x5f, 0x75, 0x75, 0x69, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x6f, 0x77, 0x6e, 0x65, 0x72, 0x55, 0x75, 0x69, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x77, 0x61, 0x74,
	0x74, 0x61, 0x67, 0x65, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x01, 0x52, 0x07, 0x77, 0x61, 0x74, 0x74,
	0x61, 0x67, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x76, 0x6f, 0x6c, 0x74, 0x61, 0x67, 0x65, 0x18, 0x0d,
	0x20, 0x01, 0x28, 0x01, 0x52, 0x07, 0x76, 0x6f, 0x6c, 0x74, 0x61, 0x67, 0x65, 0x12, 0x18, 0x0a,
	0x07, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x01, 0x52, 0x07,
	0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x22, 0x6a, 0x0a, 0x0f, 0x45, 0x6c, 0x65, 0x63, 0x74,
	0x72, 0x69, 0x63, 0x69, 0x74, 0x79, 0x43, 0x6f, 0x73, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x6f, 0x77,
	0x6e, 0x65, 0x72, 0x5f, 0x75, 0x75, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x6f, 0x77, 0x6e, 0x65, 0x72, 0x55, 0x75, 0x69, 0x64, 0x12, 0x38, 0x0a, 0x09, 0x74, 0x69, 0x6d,
	0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54,
	0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74,
	0x61, 0x6d, 0x70, 0x42, 0x4c, 0x5a, 0x4a, 0x62, 0x69, 0x74, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74,
	0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x73, 0x65, 0x6e, 0x73, 0x6f, 0x68, 0x69, 0x76, 0x65, 0x70, 0x72,
	0x6f, 0x6a, 0x65, 0x63, 0x74, 0x73, 0x2f, 0x64, 0x61, 0x74, 0x61, 0x2d, 0x73, 0x74, 0x72, 0x75,
	0x63, 0x74, 0x75, 0x72, 0x65, 0x73, 0x2f, 0x67, 0x6f, 0x6c, 0x61, 0x6e, 0x67, 0x2f, 0x6d, 0x61,
	0x74, 0x75, 0x72, 0x69, 0x78, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2f, 0x64, 0x61, 0x74,
	0x61, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_maturix_models_data_noise_proto_rawDescOnce sync.Once
	file_maturix_models_data_noise_proto_rawDescData = file_maturix_models_data_noise_proto_rawDesc
)

func file_maturix_models_data_noise_proto_rawDescGZIP() []byte {
	file_maturix_models_data_noise_proto_rawDescOnce.Do(func() {
		file_maturix_models_data_noise_proto_rawDescData = protoimpl.X.CompressGZIP(file_maturix_models_data_noise_proto_rawDescData)
	})
	return file_maturix_models_data_noise_proto_rawDescData
}

var file_maturix_models_data_noise_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_maturix_models_data_noise_proto_goTypes = []interface{}{
	(*Electricity)(nil),           // 0: maturix.models.data.Electricity
	(*ElectricityCost)(nil),       // 1: maturix.models.data.ElectricityCost
	(*timestamppb.Timestamp)(nil), // 2: google.protobuf.Timestamp
}
var file_maturix_models_data_noise_proto_depIdxs = []int32{
	2, // 0: maturix.models.data.Electricity.timestamp:type_name -> google.protobuf.Timestamp
	2, // 1: maturix.models.data.ElectricityCost.timestamp:type_name -> google.protobuf.Timestamp
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_maturix_models_data_noise_proto_init() }
func file_maturix_models_data_noise_proto_init() {
	if File_maturix_models_data_noise_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_maturix_models_data_noise_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Electricity); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_maturix_models_data_noise_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ElectricityCost); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_maturix_models_data_noise_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_maturix_models_data_noise_proto_goTypes,
		DependencyIndexes: file_maturix_models_data_noise_proto_depIdxs,
		MessageInfos:      file_maturix_models_data_noise_proto_msgTypes,
	}.Build()
	File_maturix_models_data_noise_proto = out.File
	file_maturix_models_data_noise_proto_rawDesc = nil
	file_maturix_models_data_noise_proto_goTypes = nil
	file_maturix_models_data_noise_proto_depIdxs = nil
}
