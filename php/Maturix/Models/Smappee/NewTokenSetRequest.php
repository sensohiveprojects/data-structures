<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: maturix/models/smappee.proto

namespace Maturix\Models\Smappee;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>maturix.models.smappee.NewTokenSetRequest</code>
 */
class NewTokenSetRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     */
    protected $timestamp = null;
    /**
     * Generated from protobuf field <code>string owner_uuid = 10;</code>
     */
    protected $owner_uuid = '';
    /**
     * Generated from protobuf field <code>.maturix.models.smappee.SmappeeTokenSet token_set = 11;</code>
     */
    protected $token_set = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\Timestamp $timestamp
     *     @type string $owner_uuid
     *     @type \Maturix\Models\Smappee\SmappeeTokenSet $token_set
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Maturix\Models\Smappee::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     * @return \Google\Protobuf\Timestamp
     */
    public function getTimestamp()
    {
        return isset($this->timestamp) ? $this->timestamp : null;
    }

    public function hasTimestamp()
    {
        return isset($this->timestamp);
    }

    public function clearTimestamp()
    {
        unset($this->timestamp);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     * @param \Google\Protobuf\Timestamp $var
     * @return $this
     */
    public function setTimestamp($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Timestamp::class);
        $this->timestamp = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string owner_uuid = 10;</code>
     * @return string
     */
    public function getOwnerUuid()
    {
        return $this->owner_uuid;
    }

    /**
     * Generated from protobuf field <code>string owner_uuid = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setOwnerUuid($var)
    {
        GPBUtil::checkString($var, True);
        $this->owner_uuid = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.maturix.models.smappee.SmappeeTokenSet token_set = 11;</code>
     * @return \Maturix\Models\Smappee\SmappeeTokenSet
     */
    public function getTokenSet()
    {
        return isset($this->token_set) ? $this->token_set : null;
    }

    public function hasTokenSet()
    {
        return isset($this->token_set);
    }

    public function clearTokenSet()
    {
        unset($this->token_set);
    }

    /**
     * Generated from protobuf field <code>.maturix.models.smappee.SmappeeTokenSet token_set = 11;</code>
     * @param \Maturix\Models\Smappee\SmappeeTokenSet $var
     * @return $this
     */
    public function setTokenSet($var)
    {
        GPBUtil::checkMessage($var, \Maturix\Models\Smappee\SmappeeTokenSet::class);
        $this->token_set = $var;

        return $this;
    }

}

