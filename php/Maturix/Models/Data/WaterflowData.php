<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: maturix/models/data/waterflow.proto

namespace Maturix\Models\Data;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>maturix.models.data.WaterflowData</code>
 */
class WaterflowData extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     */
    protected $timestamp = null;
    /**
     * Generated from protobuf field <code>string owner_uuid = 10;</code>
     */
    protected $owner_uuid = '';
    /**
     * Generated from protobuf field <code>string device_uuid = 11;</code>
     */
    protected $device_uuid = '';
    /**
     * Generated from protobuf field <code>double flow_value = 12;</code>
     */
    protected $flow_value = 0.0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\Timestamp $timestamp
     *     @type string $owner_uuid
     *     @type string $device_uuid
     *     @type float $flow_value
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Maturix\Models\Data\Waterflow::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     * @return \Google\Protobuf\Timestamp
     */
    public function getTimestamp()
    {
        return isset($this->timestamp) ? $this->timestamp : null;
    }

    public function hasTimestamp()
    {
        return isset($this->timestamp);
    }

    public function clearTimestamp()
    {
        unset($this->timestamp);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     * @param \Google\Protobuf\Timestamp $var
     * @return $this
     */
    public function setTimestamp($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Timestamp::class);
        $this->timestamp = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string owner_uuid = 10;</code>
     * @return string
     */
    public function getOwnerUuid()
    {
        return $this->owner_uuid;
    }

    /**
     * Generated from protobuf field <code>string owner_uuid = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setOwnerUuid($var)
    {
        GPBUtil::checkString($var, True);
        $this->owner_uuid = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string device_uuid = 11;</code>
     * @return string
     */
    public function getDeviceUuid()
    {
        return $this->device_uuid;
    }

    /**
     * Generated from protobuf field <code>string device_uuid = 11;</code>
     * @param string $var
     * @return $this
     */
    public function setDeviceUuid($var)
    {
        GPBUtil::checkString($var, True);
        $this->device_uuid = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>double flow_value = 12;</code>
     * @return float
     */
    public function getFlowValue()
    {
        return $this->flow_value;
    }

    /**
     * Generated from protobuf field <code>double flow_value = 12;</code>
     * @param float $var
     * @return $this
     */
    public function setFlowValue($var)
    {
        GPBUtil::checkDouble($var);
        $this->flow_value = $var;

        return $this;
    }

}

