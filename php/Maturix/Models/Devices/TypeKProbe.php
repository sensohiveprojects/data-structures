<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: maturix/models/devices/gaia_300.proto

namespace Maturix\Models\Devices;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>maturix.models.devices.TypeKProbe</code>
 */
class TypeKProbe extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     */
    protected $timestamp = null;
    /**
     * Generated from protobuf field <code>string device_id = 10;</code>
     */
    protected $device_id = '';
    /**
     * Generated from protobuf field <code>int32 probe_index = 11;</code>
     */
    protected $probe_index = 0;
    /**
     * Generated from protobuf field <code>float raw_probe_value = 12;</code>
     */
    protected $raw_probe_value = 0.0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\Timestamp $timestamp
     *     @type string $device_id
     *     @type int $probe_index
     *     @type float $raw_probe_value
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Maturix\Models\Devices\Gaia300::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     * @return \Google\Protobuf\Timestamp
     */
    public function getTimestamp()
    {
        return isset($this->timestamp) ? $this->timestamp : null;
    }

    public function hasTimestamp()
    {
        return isset($this->timestamp);
    }

    public function clearTimestamp()
    {
        unset($this->timestamp);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Timestamp timestamp = 1;</code>
     * @param \Google\Protobuf\Timestamp $var
     * @return $this
     */
    public function setTimestamp($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Timestamp::class);
        $this->timestamp = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string device_id = 10;</code>
     * @return string
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * Generated from protobuf field <code>string device_id = 10;</code>
     * @param string $var
     * @return $this
     */
    public function setDeviceId($var)
    {
        GPBUtil::checkString($var, True);
        $this->device_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 probe_index = 11;</code>
     * @return int
     */
    public function getProbeIndex()
    {
        return $this->probe_index;
    }

    /**
     * Generated from protobuf field <code>int32 probe_index = 11;</code>
     * @param int $var
     * @return $this
     */
    public function setProbeIndex($var)
    {
        GPBUtil::checkInt32($var);
        $this->probe_index = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>float raw_probe_value = 12;</code>
     * @return float
     */
    public function getRawProbeValue()
    {
        return $this->raw_probe_value;
    }

    /**
     * Generated from protobuf field <code>float raw_probe_value = 12;</code>
     * @param float $var
     * @return $this
     */
    public function setRawProbeValue($var)
    {
        GPBUtil::checkFloat($var);
        $this->raw_probe_value = $var;

        return $this;
    }

}

